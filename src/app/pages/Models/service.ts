export interface IService {
  id: number,
  user_id: number,
  service: string,
  rating: string,
  created_at: Date,
  updated_at: Date
}
