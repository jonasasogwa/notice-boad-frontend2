export interface IPost {
  id: number,
  user_id: number,
  post: string,
  surname: string,
  first_name: string,
  photo_url: string,
  created_at: Date,
  updated_at: Date
}
