import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/Services/services.service';
import { TokenService } from 'src/app/Services/token.service';
import { MessageService } from 'src/app/Services/message.service';

@Component({
  selector: 'app-messagespage',
  templateUrl: './messagespage.component.html',
  styleUrls: ['./messagespage.component.scss']
})
export class MessagespageComponent implements OnInit {

  posts:any = [];
  comments:any = [];
  userData:any = [];

  public comment_form = {
    post_id : null,
    comment : null,
    user_id : null,
  }

  public post_form = {
    user_id : null,
    post : null
  }

  constructor(
    private service: ServicesService,
    private token: TokenService,
    private msg : MessageService
  ) { }

  ngOnInit(): void {
    this.userData = localStorage.getItem('userData');
    this.userData = JSON.parse(this.userData);
    this.userData = this.userData.result;

    this.posts = this.getPosts();

    //do auto update
    /*this.msg.getMessage().subscribe((data)=>{
      this.getComments(data)

    });*/

    //check if token has expired
    this.token.cleanUp()

  }

  async getPosts(){
    this.service.getPosts().subscribe(
      data => {
        this.posts = data['results'];
      }
    );
  }

  async getComments(data){
    this.comment_form.post_id = data;
    this.comment_form.user_id = this.userData.id;

    this.service.getComments(data).subscribe(
      data => {
        this.comments = data;
      }
    )
  }



  async handleResponse(data){
    this.msg.setMessage("something added")
    if(data.success===1){
      return data;
    }else{
      console.log(data)
    }
  }

  async handleCommentResponse(data){
    this.msg.setMessage("something added")
    this.getComments(data)
  }


  async onSubmit(){
    this.service.makeComment(this.comment_form)
        .subscribe(data =>
          {
            this.comments.push(data)
            this.handleCommentResponse(this.comment_form.post_id)
          }
        )
  }

  async onSubmitPost(){
    this.post_form.user_id = this.userData.id;
    this.service.createPost(this.post_form)
        .subscribe(
          data => {
            this.posts.push(data);
            this.getPosts()
          }
        )
  }

}
