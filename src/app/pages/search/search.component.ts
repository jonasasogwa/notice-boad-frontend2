import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/Services/services.service';
import { Subject, throwError } from 'rxjs';
import { map, debounceTime, distinctUntilChanged, switchMap, catchError, switchMapTo, retry } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TokenService } from 'src/app/Services/token.service';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  public userData:any = [];
  public searchTerm = new Subject<string>();
  public loading: boolean;
  public searchResults: any;
  public paginationElements :any;
  public errorMessage: any;

  page: number = 1;

  public form = {
    search: null
  }

  public request_form = {
    service_id:null,
    accepting_user_id:null,
    request_user_id:null,
    request_message:null,
    service:null,
    first_name:null
  }

  isCollapsed = true;
  constructor(
    private service : ServicesService,
    private token : TokenService
  ) {}

  baseUrl = "http://localhost:3000/";

  ngOnInit() {

    this.userData = localStorage.getItem('userData');
    this.userData = JSON.parse(this.userData);
    this.userData = this.userData.result;
    this.request_form.request_user_id = this.userData.id;

    this.getSearch();

    //check if token has expired
    this.token.cleanUp()

    var body = document.getElementsByTagName("body")[0];
    body.classList.add("landing-page");
  }


  public getSearch(){
    this.searchTerm.pipe(
      map((e: any) => {
        return e.target.value
      }),
      debounceTime(400),
      distinctUntilChanged(),
      switchMap( term => {
        this.loading = true;
        return this.service._searchServices(term);
      }),
      catchError((e) => {
        this.loading = false;
        this.errorMessage = e.message;
        return throwError(e);
      }),
    ).subscribe(data => {
      this.loading = true;
      this.searchResults = data;
      this.paginationElements = this.searchResults;
      console.log(this.paginationElements)
    })
  }


  openForm() {
    document.getElementById("myForm").style.display = "block";
  }


  openForm2(accepting_user_id,service_id,service,first_name) {
    document.getElementById("myForm").style.display = "block";
    this.request_form.accepting_user_id = accepting_user_id;
    this.request_form.service_id=service_id;
    this.request_form.first_name = first_name;
    this.request_form.service = service;
    document.getElementById("name_msg").innerHTML = service+ " From "+ first_name
  }

  onSubmit(){
    this.service.requestService(this.request_form).subscribe(
      data => this.handleResponse(data)
    );
  }

  handleResponse(data){
    //this.msg.setMessage("something added")
    console.log(data)
    if(data.success===1){
      alert(data.message)
    }else{
      console.log(data)
    }
  }

  closeForm() {
    document.getElementById("myForm").style.display = "none";
  }

  star(star){
    if(star > 0){
      document.getElementById("star1").style.display = "none";
    }
  }

  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("landing-page");

  }



}
