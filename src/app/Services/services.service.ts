import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, empty } from 'rxjs';
import { IService } from '../pages/Models/service';
import { TokenService } from './token.service';
import { IPost } from '../pages/Models/post';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  private baseUrl = 'http://localhost:3000/api';

 // public userData:any = [];
  public  id = null;
  public searchResults: any;
  public commentResults: any;
  public makeCommentResults: any;

  constructor(
      private http: HttpClient,
      private token:TokenService
    ) {
    /*this.userData = localStorage.getItem('userData');
    this.userData = JSON.parse(this.userData);
    this.userData = this.userData.result;*/
  }

  register(data){
    return this.http.post(`${this.baseUrl}/user/register`, data);
  }

  login(data){
    return this.http.post(`${this.baseUrl}/user/login`, data);
  }

  sendPasswordResendLink(data){
    return this.http.post(`${this.baseUrl}/sendPasswordResetLink`, data);
  }

  changePassword(data){
    return this.http.post(`${this.baseUrl}/resetPassword`, data);
  }

  getServicesByUserId(data) :Observable<IService[]>{
    return this.http.get<IService[]>(`${this.baseUrl}/service/${data}`,
        {
          headers: new HttpHeaders().set('Authorization', `Bearer ${this.token.get()}`)
        })
  }

  addService(data) : Observable<IService[]>{
    return this.http.post<IService[]>(`${this.baseUrl}/service/create`, data,
        {
          headers: new HttpHeaders().set('Authorization', `Bearer ${this.token.get()}`)
        })
  }

  saveImage(
    id,
    Image:File
  ){
    const url = `${this.baseUrl}/user/imageUpload/${id}`;
    let input = new FormData();
    input.append('streamfile', Image);
    input.append(id, id);
    return this.http.post(url, input);
  }

  profileUpdate(data){
    return this.http.post(`${this.baseUrl}/user/update`, data,
        {
          headers: new HttpHeaders().set('Authorization', `Bearer ${this.token.get()}`)
        }
      );
  }

  getPosts() :Observable<IPost[]>{
    return this.http.get<IPost[]>(`${this.baseUrl}/post-comment/posts`,
        {
          headers: new HttpHeaders().set('Authorization', `Bearer ${this.token.get()}`)
        })
  }

  createPost(data) :Observable<any> {
    return this.http.post(`${this.baseUrl}/post-comment/post/`,data,
        {
          headers: new HttpHeaders().set('Authorization', `Bearer ${this.token.get()}`)
        })
  }

  getComments(data) :Observable<any>{
    let params = {q: data }
    return this.http.get(`${this.baseUrl}/post-comment/comments/${data}`,
        {
          headers: new HttpHeaders().set('Authorization', `Bearer ${this.token.get()}`)
        }).pipe(
          map( response => {
            return this.commentResults = response["results"];
          })
        )
  }

  makeComment(data) :Observable<any>{
    return this.http.post(`${this.baseUrl}/post-comment/comment/`,data,
        {
          headers: new HttpHeaders().set('Authorization', `Bearer ${this.token.get()}`)
        })
  }

  getAllSearchServices(data) :Observable<any>{
    let params = {q: data }
    return this.http.get(`${this.baseUrl}/service/search/${data}`,
        {
          headers: new HttpHeaders().set('Authorization', `Bearer ${this.token.get()}`)
        }).pipe(
          map( response => {
            return this.searchResults = response["data"];
          })
        )
  }

  //return the response
  public _searchServices(data){
    return this.getAllSearchServices(data);
  }

  requestService(data) {
    return this.http.post(`${this.baseUrl}/service/request_service`, data,
        {
          headers: new HttpHeaders().set('Authorization', `Bearer ${this.token.get()}`)
        }
      );
  }

  getNewRequests(data) {
    return this.http.get(`${this.baseUrl}/service/new_requests/${data}`,
        {
          headers: new HttpHeaders().set('Authorization', `Bearer ${this.token.get()}`)
        }
      );
  }

  acceptRequest(data) {
    return this.http.post(`${this.baseUrl}/service/accept_request`, data,
        {
          headers: new HttpHeaders().set('Authorization', `Bearer ${this.token.get()}`)
        }
      );
  }

  pay(data) {
    console.log(data)
    return this.http.post(`${this.baseUrl}/subscriber/create`, data,
        {
          headers: new HttpHeaders().set('Authorization', `Bearer ${this.token.get()}`)
        }
      );
  }

  getMyRequests(data) {
    return this.http.get(`${this.baseUrl}/service/myrequests/${data}`,
          {
            headers: new HttpHeaders().set('Authorization', `Bearer ${this.token.get()}`)
          }
        );
  }

}
