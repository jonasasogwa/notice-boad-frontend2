import { Component, OnInit, OnDestroy, HostListener } from "@angular/core";
import { ServicesService } from 'src/app/Services/services.service';
import { TokenService } from 'src/app/Services/token.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/Services/auth.service';

@Component({
  selector: "app-registerpage",
  templateUrl: "registerpage.component.html"
})
export class RegisterpageComponent implements OnInit, OnDestroy {

  public form = {
    email: null,
    password : null,
    surname : "App",
    first_name : null,
    gender : "Male",
    phone_no : "+491",
    user_type : "student",
    address : "Place-Holder-Str, 2",
    photo_url : "averta.png"
  };

  public error:any = {};
  loggedIn: boolean;

  constructor(
    private service: ServicesService,
    private token: TokenService,
    private router: Router,
    private auth:AuthService
  ) {}

  onSubmit() {
    this.service.register(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  handleResponse(data){
    if(data.success===1){
      alert(data.message)
    }else{
      alert(data.message)
    }
    let user = JSON.parse(atob(data.token.split('.')[1]));
    localStorage.setItem('userData', JSON.stringify(user));
    this.token.handle(data.token);
    this.auth.changeAuthStatus(true);
    this.router.navigateByUrl('/profile');
  }

  handleError(error){
    alert('Please note the following: \n Email must be a uni-rostock addess\n Password must be upto 6-character length\nUsername must not be empty');
  }


  isCollapsed = true;
  focus;
  focus1;
  focus2;


  ngOnInit() {
    this.auth.authStatus.subscribe(value => this.loggedIn=value);
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("register-page");
  }
}
