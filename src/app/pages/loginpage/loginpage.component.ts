import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/Services/token.service';
import { ServicesService } from 'src/app/Services/services.service';
import { AuthService } from 'src/app/Services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.scss']
})
export class LoginpageComponent implements OnInit {

  public form = {
    email: null,
    password : null
  };

  public error:any = {};
  public success ;
  loggedIn: boolean;

  constructor(
    private service: ServicesService,
    private token: TokenService,
    private router: Router,
    private auth:AuthService
  ) {}

  ngOnInit(): void {
    this.auth.authStatus.subscribe(value => this.loggedIn=value);
  }

  onSubmit() {
    this.service.login(this.form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  handleResponse(data){
    try{
      let user = JSON.parse(atob(data.token.split('.')[1]));
      localStorage.setItem('userData', JSON.stringify(user));
      this.token.handle(data.token);
      this.auth.changeAuthStatus(true);
      this.router.navigateByUrl('/search');
    }
    catch(Error){
        alert('Invalid Credentials')
    }
  }

  handleError(error){
    alert(error)
  }




  isCollapsed = true;
  focus;
  focus1;
  focus2;


  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("register-page");
  }

}
