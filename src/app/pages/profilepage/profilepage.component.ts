import { Component, OnInit, OnDestroy } from "@angular/core";
import { ServicesService } from 'src/app/Services/services.service';
import { AuthService } from 'src/app/Services/auth.service';
import { Router } from '@angular/router';
import { TokenService } from 'src/app/Services/token.service';
import { MessageService } from 'src/app/Services/message.service';

@Component({
  selector: "app-profilepage",
  templateUrl: "profilepage.component.html",
  styleUrls: ['./profilepage.component.scss']
})
export class ProfilepageComponent implements OnInit, OnDestroy {
  isCollapsed = true;

  public userData:any = [];
  public loggedIn : boolean;
  public userServices:any = [];
  public newRequests:any = [];

  public form = {
    id : null,
    email: null,
    first_name: null,
    surname: null,
    gender : null,
    phone_no : null,
    user_type: null,
    address: null,
    photo_url: null,
    about_me: null
  };

  public service_form = {
    user_id: null,
    service: null
  }

  public pay_form = {
    user_id: null,
    amount: 10,
    is_subscribed: 1
  }

  baseUrl = "http://localhost:3000";
  fileToUpload : File = null;
  public error:any = {};



  constructor(
    private service:ServicesService,
    private auth : AuthService,
    private router : Router,
    private token : TokenService,
    private msg : MessageService
  ) {}

  ngOnInit() {

    // initialize form from storage
    this.userData = localStorage.getItem('userData');
    this.userData = JSON.parse(this.userData);
    this.userData = this.userData.result;
    this.form.id = this.userData.id;
    this.form.email = this.userData.email;
    this.form.surname = this.userData.surname;
    this.form.first_name = this.userData.first_name;
    this.form.gender = this.userData.gender;
    this.form.phone_no = this.userData.phone_no;
    this.form.user_type = this.userData.user_type;
    this.form.photo_url = this.baseUrl+"/"+this.userData.photo_url;
    this.form.address = this.userData.address;
    this.form.about_me = this.userData.about_me;

    // initialize service forme from storage
    this.service_form.user_id = this.userData.id;

    //get user services
    this.getServicesByUserId();

    //do auto update
    this.msg.getMessage().subscribe((data)=>{
        this.getServicesByUserId();

    });

    this.getNewRequests();

    this.auth.authStatus.subscribe(value => this.loggedIn=value);

    //check if token has expired
    this.token.cleanUp()

    var body = document.getElementsByTagName("body")[0];
    body.classList.add("profile-page");
    var slider = document.getElementById("sliderRegular");
  }

  onSubmit(){
    this.service.profileUpdate(this.form ).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }

  handleResponse(data){
    this.msg.setMessage("something added")
    if(data.success===1){
      alert(data.message)
    }else{
      console.log(data)
    }
  }

  handleError(error){
    //this.error = error.error.errors;
    //console.log(this.error);
  }

  onServiceSubmit(){
    this.service.addService(this.service_form).subscribe(
      data => this.handleResponse(data),
      error => this.handleError(error)
    );
  }


  getServicesByUserId(){
    this.service.getServicesByUserId(this.userData.id)
      .subscribe( data => {
        this.userServices.results = data['results'];
    });
  }

  getNewRequests(){
    this.service.getNewRequests(this.userData.id)
      .subscribe( data => {
        this.newRequests.results = data['results'];
      })
  }

  handleFileInput(file : FileList){
    this.fileToUpload = file.item(0);
    //show image preview
    let reader = new FileReader();
    reader.onload = (event:any) => {
      this.form.photo_url = event.target.result;

    }
    reader.readAsDataURL(this.fileToUpload);
  }

  onPhotoSubmit(){
    this.service.saveImage(this.userData.id,this.fileToUpload).subscribe(
      data => this.handleIResponse(data)
    )
  }

  handleIResponse(data){
    console.log(data)
  }

  onPaySubmit(){
    this.pay_form.user_id = this.userData.id;
    this.pay_form.is_subscribed = 1;
    this.pay_form.amount = 10;
    this.service.pay(this.pay_form).subscribe(
      data => this.handlePayResponse(data)
    )
  }
  handlePayResponse(data){
    alert(data.message)
  }
  logout(event: MouseEvent){
    event.preventDefault();
    this.auth.changeAuthStatus(false);
    this.token.remove();
    this.token.removeUser();
    localStorage.clear();
    this.router.navigateByUrl('/login');
  }

  scrollToDownload(element: any) {
    element.scrollIntoView({ behavior: "smooth" });
  }

  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("index-page");

  }

}
