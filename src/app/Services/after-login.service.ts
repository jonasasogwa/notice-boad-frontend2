import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class AfterLoginService implements CanActivate {

  constructor(private token: TokenService) { }

  path: ActivatedRouteSnapshot[];
  route: ActivatedRouteSnapshot;

  /*canActivate(route: ActivatedRouteSnapshot[], state:RouterStateSnapshot): boolean |
  Observable<boolean> | Promise<boolean> {
    return this.token.loggedIn();
  }*/

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.token.loggedIn();
  }


}
