import { Component, OnInit, OnDestroy } from '@angular/core';
//import noUiSlider from "nouislider";
import { AuthService } from 'src/app/Services/auth.service';
import { Router } from '@angular/router';
import { TokenService } from 'src/app/Services/token.service';
import { ServicesService } from 'src/app/Services/services.service';

@Component({
  selector: 'app-navpage',
  templateUrl: './navpage.component.html'
})

export class NavpageComponent implements OnInit, OnDestroy {
  isCollapsed = true;
  focus;
  focus1;
  focus2;
  date = new Date();
  pagination = 3;
  pagination1 = 1;

  public userData:any = [];
  public loggedIn : boolean;
  public userServices:any = [];
  public newRequests:any = [];


  constructor(
    private auth : AuthService,
    private router : Router,
    private token : TokenService,
    private service : ServicesService
  ) {}



  logout(event: MouseEvent){
    event.preventDefault();
    this.auth.changeAuthStatus(false);
    this.token.remove();
    this.token.removeUser();
    localStorage.clear();
    this.router.navigateByUrl('/login');
  }


  ngOnInit() {
    this.userData = localStorage.getItem('userData');
    this.userData = JSON.parse(this.userData);
    this.userData = this.userData.result;

    this.auth.authStatus.subscribe(value => this.loggedIn=value);

    this.getNewRequests();

    var body = document.getElementsByTagName("body")[0];
    body.classList.add("index-page");
    var slider = document.getElementById("sliderRegular");
  }

  getNewRequests(){
    this.service.getNewRequests(this.userData.id)
      .subscribe( data => {
        this.newRequests.results = data['results'];
      })
  }

  scrollToDownload(element: any) {
    element.scrollIntoView({ behavior: "smooth" });
  }

  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("index-page");

  }

}
