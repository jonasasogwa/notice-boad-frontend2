import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/Services/services.service';

@Component({
  selector: 'app-requestpage',
  templateUrl: './requestpage.component.html',
  styleUrls: ['./requestpage.component.scss']
})
export class RequestpageComponent implements OnInit {

  public userData:any = [];
  public loggedIn : boolean;
  public userServices:any = [];
  public newRequests:any = [];

  public accept_form = {
    id : null,
    accept_message : null,
    request_message : null
  }

  constructor(
    private service : ServicesService
  ) { }

  ngOnInit(): void {
    this.userData = localStorage.getItem('userData');
    this.userData = JSON.parse(this.userData);
    this.userData = this.userData.result;

    this.getNewRequests();
  }

  getNewRequests(){
    this.service.getNewRequests(this.userData.id)
      .subscribe( data => {
        this.newRequests = data['results'];
      })
  }

  openForm(id,accept_message,request_message){
    this.accept_form.id = id;
    this.accept_form.accept_message = accept_message;
    this.accept_form.request_message = request_message;
    document.getElementById("name_msg").innerHTML = request_message
  }

  onSubmit(){
    this.service.acceptRequest(this.accept_form)
        .subscribe( data => this.handleResponse(data) )
  }

  handleResponse(data){
    //this.msg.setMessage("something added")
    console.log(data)
    if(data.success===1){
      alert(data.message)
    }else{
      console.log(data)
    }
  }

}
