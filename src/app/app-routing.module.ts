import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { Routes, RouterModule } from "@angular/router";

import { IndexComponent } from "./pages/index/index.component";
import { ProfilepageComponent } from "./pages/profilepage/profilepage.component";
import { RegisterpageComponent } from "./pages/registerpage/registerpage.component";
import { LandingpageComponent } from "./pages/landingpage/landingpage.component";
import { LoginpageComponent } from './pages/loginpage/loginpage.component';
import { SearchComponent } from './pages/search/search.component';
import { BeforeLoginService } from './Services/before-login.service';
import { AfterLoginService } from './Services/after-login.service';
import { MessagespageComponent } from './pages/messagespage/messagespage.component';
import { RequestpageComponent } from './pages/requestpage/requestpage.component';
import { MessagelogComponent } from './pages/messagelog/messagelog.component';


const routes: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  {
    path: "home",
    component: IndexComponent
  },
  {
    path: "profile",
    component: ProfilepageComponent,
    canActivate: [AfterLoginService]
  },
  {
    path: "register",
    component: RegisterpageComponent,
    canActivate: [BeforeLoginService]
  },
  {
    path: "landing",
    component: LandingpageComponent ,
    canActivate: [AfterLoginService]
  },
  {
    path: "login",
    component: LoginpageComponent,
    canActivate: [BeforeLoginService]
  },
  {
    path: "search",
    component: SearchComponent ,
    canActivate: [AfterLoginService]
  },
  {
    path: "message",
    component: MessagespageComponent ,
    canActivate: [AfterLoginService]
  },
  {
    path: "requests",
    component: RequestpageComponent ,
    canActivate: [AfterLoginService]
  },
  {
    path: "messagelog",
    component: MessagelogComponent ,
    canActivate: [AfterLoginService]
  }

];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  exports: []
})
export class AppRoutingModule {}
