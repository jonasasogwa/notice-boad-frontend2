import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/Services/services.service';

@Component({
  selector: 'app-messagelog',
  templateUrl: './messagelog.component.html',
  styleUrls: ['./messagelog.component.scss']
})
export class MessagelogComponent implements OnInit {
  public userData:any = [];
  public myRequest:any = [];

  constructor(
    private service : ServicesService
  ) { }

  ngOnInit(): void {
    this.userData = localStorage.getItem('userData');
    this.userData = JSON.parse(this.userData);
    this.userData = this.userData.result;

    this.myRequests();
  }

  myRequests(){
    this.service.getMyRequests(this.userData.id)
      .subscribe( data => {
        this.myRequest = data['results'];
      })
  }


}
