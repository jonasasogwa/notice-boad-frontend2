import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  private iss = {
    login : 'http://localhost:3000/api/user/login',
    register : 'http://localhost:3000/api/user/register'
  }

  public userData:any = [];

  constructor(
    private router : Router,
  ) { }

  handle(token){
    this.set(token)
  }

  set(token){
    localStorage.setItem('token', token);
  }

  get(){
    return localStorage.getItem('token');
  }

  remove(){
    return localStorage.removeItem('token');
  }

  removeUser(){
    return localStorage.removeItem('userData');
  }

  cleanUp(){
    let now:number;
     now =  new Date().getTime() / 1000;
    this.userData = localStorage.getItem('userData');
    this.userData = JSON.parse(this.userData);
    const expiration = this.userData.exp;
    if(expiration < now){
      this.remove();
      this.removeUser();
      this.router.navigateByUrl('/login');
    }
  }


  isValid(){
    const token = this.get();

    if(token){
      const payLoad = this.payLoad(token);
      //console.log(payLoad)
      if(payLoad.iss = ('http://localhost:3000/api/user/login' || 'http://localhost:3000/api/user/register')){
        //return Object.values(this.iss).indexOf(payLoad.iss) > -1 ? true:false;
        return true
      }
    }

    return false;
  }

  payLoad(token){
    const payLoad = token.split('.')[1];
    return this.decode(payLoad);
  }

  decode(payLoad){
    return JSON.parse(atob(payLoad));
  }

  loggedIn(){
    return this.isValid();
  }


}
